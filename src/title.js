(function(ctpl,$,window){
	var selector = ctpl.sel_all('title');
	var menu_width = 100;
	var menu = null;
	var menu_inner = null;
	var menu_tpl = [
		'<ul>',
			'<li style="width:30px;">OK</li>',
			'<li style="width:30px;">×</li>',
			'<li style="width:30px;">',
				'H1&nbsp;',
			'</li>',
		'</ul>'
	].join('\n');
	var menu_opt = {
		'0':function(){
			ctpl.stopEdit();
		},
		'1':function(){
			if(confirm('Delete it?')){
				ctpl.stopEdit();
				ctpl.target.parent().remove();
			}
		},
		'2':function(){

		}
	};
	$(selector).css({
		'width':ctpl.width()
	});
	var toggleActiveStatus = function(evt){
		var self = $(this);
		self.toggleClass('active');
	};
	var toggleMenu = function(){
		var self = $(this);
		if(!self.is('.editing')){
			ctpl.stopEdit();
			ctpl.target = self;
			ctpl.target.toggleClass('editing');
			ctpl.onEdit(menu_width);
		}
	};
	var onKeypress = function(evt){
		if(evt.ctrlKey && evt.keyCode == 10){
			ctpl.stopEdit();
		}else if(evt.ctrlKey && evt.shiftKey && evt.keyCode == 1){
			//该功能应该放在 ctpl 中，ctpl 处理所有组合排版
			var url = prompt('URL','http://');
			var a = $('<a>').html(url).attr('href',url);
			ctpl.insert(a[0]);
		}
	};

	var onMenuClick = function(){
		menu_opt[$(this).index()](this);
	};
	ctpl.addAllMenu(selector,{
		width:menu_width,
		template:menu_tpl,
		onMenuItemClick:onMenuClick
	});
	ctpl
	.on('mouseenter mouseleave',selector,toggleActiveStatus)
	.on('click',selector,toggleMenu)
	.on('keypress',onKeypress);
	var title = {
		selector:selector
	};
	ctpl.title = title;
	window.ctpl = ctpl;
})(window.ctpl || {},jQuery,window);
