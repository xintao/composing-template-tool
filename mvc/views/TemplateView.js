var TemplateView = Backbone.View.extend({
	tagName:'div',
	className:'ctpl-menu',
	newline:true,
	editable:true,
	autoclear:false,
	initialize:function(opt){
		this.opts = _.extend(opt);
		this.setEvents();
		this.render(this.opts);
		var self = this;
		ctpl.on('change:target',function(model,target){
			if(target){
				self.onEdit(model,target);
			}else{
				self.stopEdit(self.main);
			}
		});
		return this;
	},
	updateMenu:function(){
		this.menu = null;
	},
	renderMenu:function(view){
		this.updateMenu();
		if(this.menu)
			this.menu.render(this);
	},
	render:function(opts,refresh){
		var self = this;
		var main = $('<div></div>').addClass(self.classPrefix+'-'+ opts.type).html(refresh?this.main.html():opts.text);
		self.main = main;
		var elements = self;
		if(_.isFunction(self.render_)){
			self.render_(opts,refresh);
		}
		this.updateMenu();
		this.$el.html('').append(main);
		if(!refresh)
			if(this.opts.name == 'catalog'){
				$('[class*="'+this.classPrefix+'"]').remove();
				opts.target.prepend(this.$el);
			}else{
				opts.target.append(this.$el);
			}
		return this;
	},
	setEvents:function(){
		var me = 'mouseenter [class^="'+this.classPrefix+'"]';
		var ml = 'mouseleave [class^="'+this.classPrefix+'"]';
		var kp = 'keypress  [class^="'+this.classPrefix+'"]';
		var ku = 'keyup  [class^="'+this.classPrefix+'"]';
		var kd = 'keydown  [class^="'+this.classPrefix+'"]';
		var ck = 'tap';
		var dck = 'doubletap'
		var events = {};
		events[me] = 'toggleStatus';
		events[ml] = 'toggleStatus';
		events[kp] = 'stopText';
		events[kd] = 'checkBackSpace';
		events[ku] = 'resetText';
		events[ck] = 'toggleMenu';
		events[dck] = 'clearText';
		/*
		var dg = 'drag';
		var dgs = 'dragstart';
		var dge = 'dragend';
		var hd = 'hold';
		events[dg] = 'drag';
		events[dgs] = 'dragStart';
		events[dge] = 'dragEnd';
		events[hd] = 'hold';
		*/
		if(!this.events){
			this.events = {};
		}
		this.events = _.extend(this.events,events);
	},
	clearText:function(){
		if(this.autoclear && confirm('确认清空当前组件内容？')){
			this.main.html('').focus().click();
		}
	},
	stopText:function(e){
		var self = $(e.currentTarget);
		if(!this.newline && e.keyCode==13  || e.ctrlKey && e.keyCode==10){
			this.stopEdit(self);
			return;
		}
	},
	resetText:function(e){
		var self = $(e.currentTarget);
		this.opts.text = self.html();
	},
	checkBackSpace:function(e){
		var self = $(e.currentTarget);
		if(e.keyCode == 8 && self.find('li').text().trim()=="" && this.opts.name == 'list'){
			return false;
		}
	},
	toggleStatus:function(e){
		var self = $(e.currentTarget);
		self.toggleClass('active');
	},
	toggleMenu:function(e){
		var target = $(this.$el.find('[class^="'+this.classPrefix+'"]'));
		var self = this;
		if(!target.is('.editing')){
			self.stopEdit(ctpl.get('target'));
			ctpl.set({
				'target':target,
				'view':self
			});
		}
	},
	onEdit:function(model,target){
		if(!target)
			return;
		var self = model.get('view');
		target.addClass('editing');
		target.parent('.ctpl-menu').addClass('draggable');
		self.showMenu(target);
		if(self.editable)
			target.attr('contenteditable',true).stop().focus();
	},
	stopEdit:function(target){
		var self = this;
		if(target){
			target.removeClass('editing').removeAttr('contenteditable').stop();
			self.hideMenu(target)
		}
		ctpl.set({
			'target':undefined,
			'view':undefined
		})
	},
	hideMenu:function(target){

	},
	showMenu:function(target){
		this.renderMenu();
	},
	drag:function(e){
		var self = this.$el;
		self.addClass('dragging');
        var g = e.gesture;
        var data_top = self.data('top');
        if(!data_top){
        	data_top = self.offset().top;
        	self.data('top',data_top);
        }
        var top = data_top + g.deltaY;
        var main = self.css({
            'top':top
        });
        event.preventDefault();
    },
	dragEnd:function(e){
		var self = this.$el;
		self.removeClass('dragging');
        var g = e.gesture;
        self.css({
            'top':0
        });
        delete this.other;
        event.preventDefault();
	},
	dragStart:function(e){
		var self = this.$el;
		self.addClass('dragging');
        var other = $('.'+this.className).not(self.parent());
       	var position = {};
        var size = other.length;
        while(size--){
        	var o = other.eq(size);
        	var top = o.offset().top;
        	var key = [top,top+o.height()].join('~');
        	position[key] = o;
        }
        this.position = position;
        event.preventDefault();
	},
	hold:function(e){

	},
	onElement:function(cur,top){
		var position = this.position;
		for(var range in position){
			var r = range.split('~');
			var from = +r[0];
			var to = +r[1];
			if(top>=from && top<=top){
				var o = position[range];
				$('.draon').removeClass('dragon');
				$('.active').addClass('dragon');
			}
		}
	}
});